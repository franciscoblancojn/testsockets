//requires
const   express = require('express'),
        path = require('path'),
        bodyParser = require('body-parser'),
        SocketIO = require('socket.io');

const f = require('./functions')
/**
 * @description port and rute
 */
const port = 3001;


/**
 * @description load app 
 */
var app = express();
app.set('port', port);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(express.urlencoded({extended: false}));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

[
    "/api",,
].forEach(rute => {
    app.get(rute, async function(req, res) {
        console.log("get");
        f.respond({
            res,
            responde:{
                type:"get"
            }
        })
    });
});
app.use(express.static(path.join(__dirname,'page')))

/**
 * app.listen
 * @description enpoint listen 
 */
const server = app.listen(port, function() {
    console.log('ok ' + port);
});

const io = SocketIO(server)

io.on('connection',(socket)=>{
    console.log('connection ', socket.id);
    socket.on('sendMsj',(json)=>{
        console.log(json);
        io.to(json.idResector).emit('sendMsj',json)
        
    })

})