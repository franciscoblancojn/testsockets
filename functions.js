const respond = ({res,responde}) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(responde));
    res.end();
} 


const f = {
    respond,
}

module.exports = f